﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft;


namespace DSXAPI.Tests
{
    [TestClass]
    public class TemplateHelperTests
    {
        /// <summary>
        /// Tests the result of a ChangePin template compile sending the StudentID
        /// </summary>
        [TestMethod]
        public void ChangePinWStudentID()
        {
            var result = DSXTestsHelper.ProcessChangePin(@"{StudentID:'S1', CatCardNumber:'C1', PIN:'P1'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectChangePinWithStudentIDResult);
        }

        /// <summary>
        /// Tests the result of a ChangePin template compile without a StudentID
        /// </summary>
        [TestMethod]
        public void ChangePinNoStudentID()
        {
            var result = DSXTestsHelper.ProcessChangePin(@"{CatCardNumber:'C1', PIN:'P1'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectChangePinNoStudentIDResult);
        }


        /// <summary>
        /// Tests the result of a ReplaceCard template compile.
        /// </summary>
        [TestMethod]
        public void ReplaceCard()
        {
            var result = DSXTestsHelper.ProcessReplaceCard(@"{StudentID:'S1', CatCardNumber:'C1'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectReplaceCardResult);
        }

        /// <summary>
        /// Tests the result of a RevokeAccess template compile sending a StudentID
        /// </summary>
        [TestMethod]
        public void RevokeAccessWithStudentID()
        {
            var result = DSXTestsHelper.ProcessRevokeAccess(@"{StudentID:'S1', CatCardNumber:'C1', AccessLevel:'AL1'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectRevokeAccessWithStudentIDResult);
        }

        /// <summary>
        /// Tests the result of a RevokeAccess template compile without a StudentID.
        /// </summary>
        [TestMethod]
        public void RevokeAccessNoStudentID()
        {
            var result = DSXTestsHelper.ProcessRevokeAccess(@"{CatCardNumber:'C1', AccessLevel:'AL1'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectRevokeAccessNoStudentIDResult);
        }

        /// <summary>
        /// Tests the result of a GrantAccess template compile sending a StudentID and the Access level being in HNRV
        /// </summary>
        [TestMethod]
        public void GrantAccessWStudentIDWPINHNRV()
        {
            var result = DSXTestsHelper.ProcessGrantAccess(@"
                {StudentID: 'S1', FirstName: 'FN', LastName: 'LN', Company: 'CO', 
                CatCardNumber:'CC', AccessLevel: 'HNRV Student', PIN: 'P1',
                StartDate: '1/30/2020 15:00', EndDate: '12/31/8888 12:00'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectGrantAccessWStudentIDWPINHNRVResult);
        }

        /// <summary>
        /// Tests the result of a GrantAccess template compile with no StudentID and the Access level being CORO
        /// </summary>
        [TestMethod]
        public void GrantAccessNoStudentIDNoPINNoHNRV()
        {
            var result = DSXTestsHelper.ProcessGrantAccess(@"
                {FirstName: 'FN', LastName: 'LN', Company: 'CO', 
                CatCardNumber:'CC', AccessLevel: 'CORO Student',
                StartDate: '1/30/2020 15:00', EndDate: '12/31/8888 12:00'}");
            Assert.IsTrue(result == DSXTestsHelper.CorrectGrantAccessNoStudentIDWNoPINNoHNRVResult);
        }








    }
}
