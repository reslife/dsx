﻿
using System.IO;
using System.Web.Helpers;
using DSX;

namespace DSXAPI.Tests
{
    public static class DSXTestsHelper
    {

        /// <summary>
        /// Processes a GrantAccess template compile
        /// </summary>
        /// <param name="JsonString">The JsonString to be sent to the templating engine</param>
        /// <returns>a string with the template result</returns>
        public static string ProcessGrantAccess(string JsonString)
        {
            return ProcessFile(Path.Combine(TemplatesPath, GrantAccessTemplateFileName) , JsonString);
        }

        /// <summary>
        /// Processes a RevokeAccess template compile
        /// </summary>
        /// <param name="JsonString">The JsonString to be sent to the templating engine</param>
        /// <returns>a string with the template result</returns>
        public static string ProcessRevokeAccess(string JsonString)
        {
            return ProcessFile(Path.Combine(TemplatesPath, RevokeAccessTemplateFileName), JsonString);
        }
        /// <summary>
        /// Processes a ReplaceCard template compile
        /// </summary>
        /// <param name="JsonString">The JsonString to be sent to the templating engine</param>
        /// <returns>a string with the template result</returns>
        public static string ProcessReplaceCard(string JsonString)
        {
            return ProcessFile(Path.Combine(TemplatesPath, ReplaceCardTemplateFileName), JsonString);
        }
        /// <summary>
        /// Processes a ChangePin template compile
        /// </summary>
        /// <param name="JsonString">The JsonString to be sent to the templating engine</param>
        /// <returns>a string with the template result</returns>
        public static string ProcessChangePin(string JsonString)
        {
            return ProcessFile(Path.Combine(TemplatesPath,ChangePinTemplateFileName), JsonString);
        }
        /// <summary>
        /// Executes the template compile
        /// </summary>
        /// <param name="TemplateFileFullPath">The path to the template to be compiled</param>
        /// <param name="JsonString">The JsonString to be sent to the templating engine</param>
        /// <returns>a string with the template result</returns>
        public static string ProcessFile(string TemplateFileFullPath, string JsonString)
        {
            var th = new TemplateHelper();
            var data = Json.Decode(JsonString);
            var result = th.CompileTemplate(TemplateFileFullPath, data);
            return result;
        }

        //Read only constants

        public static readonly string TemplatesPath = @"..\..\..\DSX\Templates\";

        //Template File Paths
        public static readonly string GrantAccessTemplateFileName = "GrantAccess.txt", 
            RevokeAccessTemplateFileName = "RevokeAccess.txt",
            ChangePinTemplateFileName = "ChangePin.txt",
            ReplaceCardTemplateFileName = "ReplaceCard.txt";

        //DML Directory Paths
        public static readonly string DMLDestinationPath = @"C:\Temp\DSX\DML\", 
            DMLArchiveDestinationPath = @"C:\Temp\DSX\DMLArchive\";

        //File Names
        public static readonly string GrantAccessFileName = @"^imp_GrantAccess_{date}{time}.txt",
            RevokeAccessFileName = @"^imp_RevokeAccess_{date}{time}.txt",
            ChangePinFileName = @"^imp_ChangePin_{date}{time}.txt",
            ReplaceCardFileName = @"^imp_ReplaceCard_{date}{time}.txt";


        /// <summary>
        /// This is how the ChangePin sending a StudentID template compile result should look like
        /// </summary>
        public static readonly string CorrectChangePinWithStudentIDResult = @"I L1 U4 ^S1^^^
T Cards
F Code ^C1^^^
F PIN ^P1^^^
U";
        /// <summary>
        /// This is how the result of the ChangePin without a StudentID should look like
        /// </summary>
        public static readonly string CorrectChangePinNoStudentIDResult = @"I L1 U2 ^C1^^^
T Cards
F Code ^C1^^^
F PIN ^P1^^^
U";
        /// <summary>
        /// This is how the ReplaceCard template compile result should look like
        /// </summary>
        public static readonly string CorrectReplaceCardResult = @"I L1 U4 ^S1^^^
T UDF
F UdfNum ^2^^^
F UdfText ^C1^^^
W
T Cards
F ReplaceCode ^C1^^^
W";
        /// <summary>
        /// This is how the RevokeAccess result when sending a StudentID should look like
        /// </summary>
        public static readonly string CorrectRevokeAccessWithStudentIDResult = @"I L1 U4 ^S1^^^
T Cards
F Code ^C1^^^
F DelAcl ^AL1^^^
W";
        /// <summary>
        /// This is how the RevokeAccess result without a StudentID should look like
        /// </summary>
        public static readonly string CorrectRevokeAccessNoStudentIDResult = @"I L1 U2 ^C1^^^
T Cards
F Code ^C1^^^
F DelAcl ^AL1^^^
W";
        /// <summary>
        /// This is how the GrantAccess result, sending a StudentID and having an HNRV access plan should look like
        /// </summary>
        public static readonly string CorrectGrantAccessWStudentIDWPINHNRVResult = @"I L1 U4 ^S1^^^
T Names
F FName ^FN^^^
F LName ^LN^^^
F Company ^CO^^^
W
T UDF
F UdfNum ^2^^^
F UdfText ^CC^^^
W
T UDF
F UdfNum ^4^^^
F UdfText ^S1^^^
W
T Cards
F Code ^CC^^^
F PIN ^P1^^^
F StartDate ^1/30/2020 15:00^^^
F StopDate ^12/31/8888 12:00^^^
F NumUses ^9999^^^
F AddAcl ^HNRV Student^^^
F Loc ^28^^^
F OLL ^1^^^
W";
        /// <summary>
        /// This is how the GrantAccess result, without a StudentID and having an CORO access plan should look like
        /// </summary>
        public static readonly string CorrectGrantAccessNoStudentIDWNoPINNoHNRVResult = @"I L1 U2 ^CC^^^
T Names
F FName ^FN^^^
F LName ^LN^^^
F Company ^CO^^^
W
T UDF
F UdfNum ^2^^^
F UdfText ^CC^^^
W
T Cards
F Code ^CC^^^
F StartDate ^1/30/2020 15:00^^^
F StopDate ^12/31/8888 12:00^^^
F NumUses ^9999^^^
F AddAcl ^CORO Student^^^
W";
    }
}



