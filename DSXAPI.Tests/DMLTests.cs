﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DSX;
using System.Web.Helpers;
using System.IO;
using System.Linq;

namespace DSXAPI.Tests
{
    [TestClass]
    public class DMLTests
    {
        [TestMethod]
        public void CreateGrantAccess()
        {
            var dml = CreateDmlObject();
            var JsonString = @"{StudentID: 'S1', FirstName: 'FN', LastName: 'LN', Company: 'CO', 
                CatCardNumber:'CC', AccessLevel: 'HNRV Student', PIN: 'P1',
                StartDate: '1/30/2020 15:00', EndDate: '12/31/8888 12:00'}";
            var JsonObject = Json.Decode(JsonString);
            dml.GrantAccess(JsonObject);

            Assert.IsTrue(FileExists("*GrantAccess*"));
        }

        [TestMethod]
        public void CreateChangePin()
        {
            var dml = CreateDmlObject();
            var JsonString = @"{StudentID:'S1', CatCardNumber:'C1', PIN:'P1'}";
            var JsonObject = Json.Decode(JsonString);
            dml.ChangePin(JsonObject);
            Assert.IsTrue(FileExists("*ChangePin*"));
        }

        [TestMethod]
        public void RevokeAccess()
        {
            var dml = CreateDmlObject();
            var JsonString = @"{StudentID:'S1', CatCardNumber:'C1', AccessLevel:'AL1'}";
            var JsonObject = Json.Decode(JsonString);
            dml.RevokeAccess(JsonObject);
            Assert.IsTrue(FileExists("*RevokeAccess*"));
        }
        [TestMethod]
        public void ReplaceCard()
        {
            var dml = CreateDmlObject();
            var JsonString = @"{StudentID:'S1', CatCardNumber:'C1'}";
            var JsonObject = Json.Decode(JsonString);
            dml.ReplaceCard(JsonObject);
            Assert.IsTrue(FileExists("*ReplaceCard*"));
        }

        private bool FileExists(string SearchPattern)
        {
            var result = false;

            var DMLCreatedFiles = Directory.EnumerateFiles(DSXTestsHelper.DMLDestinationPath, SearchPattern);
            var DMLArchiveCreatedFiles = Directory.EnumerateFiles(DSXTestsHelper.DMLArchiveDestinationPath, SearchPattern);

            result = (DMLCreatedFiles.Count() > 0) && (DMLArchiveCreatedFiles.Count() > 0);

            return result;
        }

        private DML CreateDmlObject()
        {
            var result = new DML(DSXTestsHelper.DMLDestinationPath, DSXTestsHelper.DMLArchiveDestinationPath)
            {

                ChangePinFileName = DSXTestsHelper.ChangePinFileName,
                GrantAccessFileName = DSXTestsHelper.GrantAccessFileName,
                RevokeAccessFileName = DSXTestsHelper.RevokeAccessFileName,
                ReplaceCardFileName = DSXTestsHelper.ReplaceCardFileName,

                TemplatesPath = DSXTestsHelper.TemplatesPath,

                GrantAccessTemplateFileName = DSXTestsHelper.GrantAccessTemplateFileName,
                RevokeAccessTemplateFileName = DSXTestsHelper.RevokeAccessTemplateFileName,
                ChangePinTemplateFileName = DSXTestsHelper.ChangePinTemplateFileName,
                ReplaceCardTemplateFileName = DSXTestsHelper.ReplaceCardTemplateFileName,
                
            };

            return result;
        }
    }
}
