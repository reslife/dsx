﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DSXAPI;
using DSXAPI.Controllers;
using System.Web.Helpers;

namespace DSXAPI.Tests.Controllers
{
    [TestClass]
    public class DSXControllerTest
    {
        [TestMethod]
        public void GrantAccessPost()
        {
            var controller = new GrantAccessController();
            var requestBody = new HttpRequestMessage() { 
                Content = new StringContent(@"{
                    FirstName: 'John',
                    LastName: 'Doe',
                    Company: 'CORO Student',
                    CatCardNumber: '90200000000',
                    AccessLevel: 'CORO Student',
                    PIN: '0000',
                    StartDate: '1/1/2020 15:00',
                    EndDate: '12/31/8888 12:00'}")
            };
            var response = controller.Post(requestBody);
            var result = response.Content.ReadAsStringAsync().Result;

            var DecodedResult = Json.Decode(result);

            Assert.IsTrue(DecodedResult.Result.Status == "Success");
            Assert.IsTrue(DecodedResult.Result.Message == "DML File Created");

        }
    }
}
