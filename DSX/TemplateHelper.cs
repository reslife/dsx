﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HandlebarsDotNet;
using Newtonsoft;
using Newtonsoft.Json;

namespace DSX
{
    public class TemplateHelper
    {
        public TemplateHelper()
        {
            registerHandlebarsHelpers();
        }

        public string CompileTemplate(string TemplatePath, object o)
        {
            var template = File.ReadAllText(TemplatePath);
            var compiledTemplate = Handlebars.Compile(template.Replace("\t","") );
            return compiledTemplate(o);
        }

        private void registerHandlebarsHelpers()
        {

            void iff(TextWriter output, HelperOptions options, dynamic context, object[] arguments)
            {

                if (arguments.Length != 3) throw new HandlebarsException("{{eq}} helper must have exactly three argument");

                var evaluation = false;
                var left = arguments[0].ToString().ToLower();   //First Argument
                var opr = arguments[1].ToString().ToLower();    //Operator
                var right = arguments[2].ToString().ToLower();  //Second Argument

                switch (opr)
                {
                    case "==":
                        evaluation = left.Equals(right);
                        break;
                    case "=":
                        evaluation = left.Equals(right);
                        break;
                    case "equals":
                        evaluation = left.Equals(right);
                        break;
                    case "startswith":
                        evaluation = left.StartsWith(right);
                        break;
                    case "notstartswith":
                        evaluation = !left.StartsWith(right);
                        break;
                    case "endswith":
                        evaluation = left.EndsWith(right);
                        break;
                    case "notendswith":
                        evaluation = !left.EndsWith(right);
                        break;
                    case "contains":
                        evaluation = left.Contains(right);
                        break;
                    case "notcontains":
                        evaluation = !left.Contains(right);
                        break;
                    case "notequals":
                        evaluation = !left.Equals(right);
                        break;
                    case "<>":
                        evaluation = !left.Equals(right);
                        break;
                    case "!=":
                        evaluation = !left.Equals(right); 
                        break;
                    case ">":
                        evaluation = double.Parse(left) > double.Parse(right);
                        break;
                    case "<":
                        evaluation = double.Parse(left) < double.Parse(right);
                        break;
                    case ">=":
                        evaluation = double.Parse(left) >= double.Parse(right);
                        break;
                    case "=<":
                        evaluation = double.Parse(left) <= double.Parse(right);
                        break;

                    default:
                        throw new Exception($"Unknown operator \"{opr}\"");
                }

                if (evaluation)
                {
                    options.Template(output, null);
                }
                else
                {
                    options.Inverse(output, null);
                }
            }



            void NotNull(TextWriter output, HelperOptions options, dynamic context, object[] arguments)
            {

                if (arguments.Length != 1) throw new HandlebarsException("{{eq}} helper must have exactly one argument");

                var evaluation = false;
                var left = arguments[0].ToString(); //Argument

                evaluation = !string.IsNullOrEmpty(left);

                if (evaluation)
                {
                    options.Template(output, null);
                }
                else
                {
                    options.Inverse(output, null);
                }
            }

            Handlebars.RegisterHelper("iff", iff);
            Handlebars.RegisterHelper("NotNull", NotNull);

        }
    }
}
