﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace DSX
{
    /// <summary>
    /// This class is responsible of generating the output DML file using the templates as base.
    /// It is required to provide all parameters.
    /// </summary>
    public class DML
    {
        private string _FilesDestinationPath;

        public string FilesDestinationPath
        {
            get { return _FilesDestinationPath; }
        }

        private string _FilesArchiveDestinationPath;

        public string FilesArchiveDestinationPath
        {
            get { return _FilesArchiveDestinationPath; }
        }


        public string TemplatesPath { get; set; }

        public string GrantAccessTemplateFileName { get; set; }
        public string RevokeAccessTemplateFileName { get; set; }
        public string ChangePinTemplateFileName { get; set; }
        public string ReplaceCardTemplateFileName { get; set; }


        public string GrantAccessFileName { get; set; }
        public string RevokeAccessFileName { get; set; }
        public string ChangePinFileName { get; set; }
        public string ReplaceCardFileName { get; set; }

        private TemplateHelper helper { get; set; }

        public DML(string DestinationPath, string ArchiveDestinationPath)
        {
            _FilesDestinationPath = DestinationPath;
            _FilesArchiveDestinationPath = ArchiveDestinationPath;

            helper = new TemplateHelper();

            //Creating the directories if they do not exists. Otherwise nothing happens.
            Directory.CreateDirectory(_FilesDestinationPath);
            Directory.CreateDirectory(_FilesArchiveDestinationPath);
        }

        /// <summary>
        /// Processes a GrantAccess template compile and creates the DML file in the specified destination of the DML object.
        /// </summary>
        /// <param name="JsonObject">The Json Object to be sent to the templating engine</param>
        public void GrantAccess(object JsonObject)
        {
            var result = ProcessTemplate(Path.Combine(TemplatesPath, GrantAccessTemplateFileName), JsonObject);
            CreateFiles(result, GrantAccessFileName, JsonObject);
        }


        /// <summary>
        /// Processes a RevokeAccess template compile and creates the DML file in the specified destination of the DML object.
        /// </summary>
        /// <param name="JsonObject">The Json Object to be sent to the templating engine</param>
        public void RevokeAccess(object JsonObject)
        {
            var result = ProcessTemplate(Path.Combine(TemplatesPath, RevokeAccessTemplateFileName), JsonObject);
            CreateFiles(result, RevokeAccessFileName, JsonObject);
        }

        /// <summary>
        /// Processes a ChangePin template compile and creates the DML file in the specified destination of the DML object.
        /// </summary>
        /// <param name="JsonObject">The Json Object to be sent to the templating engine</param>
        public void ChangePin(object JsonObject)
        {
            var result = ProcessTemplate(Path.Combine(TemplatesPath, ChangePinTemplateFileName), JsonObject);
            CreateFiles(result, ChangePinFileName, JsonObject);
        }

        /// <summary>
        /// Processes a ReplaceCard template compile and creates the DML file in the specified destination of the DML object.
        /// </summary>
        /// <param name="JsonObject">The Json Object to be sent to the templating engine</param>
        public void ReplaceCard(object JsonObject)
        {
            var result = ProcessTemplate(Path.Combine(TemplatesPath, ReplaceCardTemplateFileName), JsonObject);
            CreateFiles(result, ReplaceCardFileName, JsonObject);
        }

        /// <summary>
        /// Creates the DML files
        /// </summary>
        /// <param name="Contents">The text contents to be added to the file.</param>
        /// <param name="FileName">Then name of the file. The filename accepts the {Date} and {Time} placeholders
        /// wich will be replaced by the current date or time. in the yyyyMMdd and HHmmss format.</param>
        /// <param name="JsonObject">The json object with the requester data.</param>
        public void CreateFiles(string Contents, string FileName, object JsonObject)
        {
            var finalFileName = ReplaceCaseInsensitive(FileName, "{Date}", DateTime.Now.ToString("yyyyMMdd"));
            finalFileName = ReplaceCaseInsensitive(finalFileName, "{Time}", DateTime.Now.ToString("HHmmssfff"));
            //If the JsonObject parameter was provided. This is to replace values in the filename in case tags were provided.
            if (JsonObject != null) {
                var dataString = JsonConvert.SerializeObject(JsonObject);
                var d = JsonConvert.DeserializeObject<Dictionary<string, object>>(dataString);
                foreach (var key in d.Keys)
                {
                    finalFileName = ReplaceCaseInsensitive(finalFileName, "{" + key + "}", d[key].ToString());
                }
            }
            //Creating the DSX DML file
            File.WriteAllText(Path.Combine(_FilesDestinationPath, finalFileName), Contents);
            //Creating a copy of the file in the archived files destination.
            File.WriteAllText(Path.Combine(_FilesArchiveDestinationPath, finalFileName), Contents);
        }


        /// <summary>
        /// Executes the template compile method.
        /// </summary>
        /// <param name="TemplateFileFullPath">The path to the template to be compiled</param>
        /// <param name="JsonObject">The Json Object to be sent to the templating engine</param>
        /// <returns>a string with the template result</returns>
        public string ProcessTemplate(string TemplateFileFullPath, object JsonObject)
        {
            var th = new TemplateHelper();
            var result = th.CompileTemplate(TemplateFileFullPath, JsonObject);
            return result;
        }


        private string ReplaceCaseInsensitive(string input, string search, string replacement)
        {
            string result = Regex.Replace(
                input,
                search,
                replacement.Replace("$", "$$"),
                RegexOptions.IgnoreCase
            );
            return result;
        }
    }
}
