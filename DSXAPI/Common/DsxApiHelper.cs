﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;

namespace DSXAPI.Common
{
    /// <summary>
    /// Class to provide common methods between controllers.
    /// </summary>
    public class DsxApiHelper
    {
        private string _RequestJsonString;
        private string _User;
        private string _RequestType;

        public enum RequestType
        {
            ChangePin,
            GrantAccess,
            ReplaceCard,
            RevokeAccess
        }

        /// <summary>
        /// This Method process a DsxRequest for any kind of RequestType
        /// </summary>
        /// <param name="Type">The type of request to be processed</param>
        /// <returns>an HttpResponseMessage with a Json object type like ApiResponseObject</returns>
        public HttpResponseMessage ProcessDsxRequest(RequestType Type)
        {
            var lastStep = "Begining of the thread.";
            _RequestType = Enum.GetName(typeof(RequestType), Type);
            try
            {
                LogInfo();
                lastStep = "Logged the info. Next Step: Created the DML Object.";
                var DML = new DSXHelper().CreateDmlObject();
                lastStep = "Created the DML Object. Next step: Decoded the Json received.";
                var JsonBody = System.Web.Helpers.Json.Decode(_RequestJsonString);
                lastStep = $"Decoded the Json received. Next step: Process the {_RequestType} request";

                switch (Type)
                {
                    case RequestType.ChangePin:
                        DML.ChangePin(JsonBody);
                        break;
                    case RequestType.GrantAccess:
                        DML.GrantAccess(JsonBody);
                        break;
                    case RequestType.ReplaceCard:
                        DML.ReplaceCard(JsonBody);
                        break;
                    case RequestType.RevokeAccess:
                        DML.RevokeAccess(JsonBody);
                        break;
                }

                lastStep = $"Process the {_RequestType} request";

                return SuccessResponse();
            }
            catch (Exception e)
            {
                logError(e, $"Error when processing the {_RequestType} request. The last step in the thread was: {lastStep}");
                return ErrorResponse(e.Message);
            }
        }

        /// <summary>
        /// This property is used to remove the carret return from the Json string objecto, otherwise the log has the new line characters sent in the Json object.
        /// </summary>
        public string RequestJsonString
        {
            get { return Regex.Replace(_RequestJsonString, @"\t|\n|\r", ""); }
            set { _RequestJsonString = value; }
        }

        /// <summary>
        /// Creates an instance of the DsxApiHelper class. This class includes common methods among DSX controllers.
        /// </summary>
        /// <param name="value">The HttpRequestMessage object received by the controller</param>
        /// <param name="User">The User processing the request</param>
        public DsxApiHelper(HttpRequestMessage value, string User)
        {
            var body = value.Content.ReadAsStringAsync().Result;
            _RequestJsonString = body;
            _User = User;
        }

        /// <summary>
        /// To access the NLog library to be able to use the logger
        /// </summary>
        private readonly NLog.Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Creates a informational log entry
        /// </summary>
        public void LogInfo()
        {
            Logger.Info($"|Module:{_RequestType}|User:{_User}|Value:{RequestJsonString}");
        }

        /// <summary>
        /// Creates an error log entry
        /// </summary>
        /// <param name="e">The exception being thrown</param>
        /// <param name="message">A message to be logged</param>
        public void logError(Exception e, string message)
        {
            Logger.Error(e, message);
        }

        /// <summary>
        /// Creates an error reposnse object to be sent to the client.
        /// </summary>
        /// <returns>An error http response message object.</returns>
        public HttpResponseMessage ErrorResponse(string message)
        {
            var response = new
            {
                Result = new
                {
                    Status = "Error",
                    User = _User,
                    RequestType = _RequestType,
                    Message = message,
                    OriginalData = _RequestJsonString
                }
            };

            return new HttpResponseMessage()
            {
                Content = new StringContent(Json.Encode(response),
                Encoding.UTF8,
                "application/json")
            };
        }

        /// <summary>
        /// Creates a Http Response object to be returned to the requester.
        /// </summary>
        /// <returns>A Http Response object with the success information in json format.</returns>
        public HttpResponseMessage SuccessResponse()
        {
            var response = new { 
                Result = new { 
                    Status = "Success",
                    User = _User,
                    RequestType = _RequestType,
                    Message = "DML File Created",
                    OriginalData = Json.Decode(_RequestJsonString)
            } };

            return new HttpResponseMessage()
            {
                Content = new StringContent(Json.Encode(response),
                Encoding.UTF8,
                "application/json")
            };
        }

    }
}