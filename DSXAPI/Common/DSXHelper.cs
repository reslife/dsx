﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DSX;

namespace DSXAPI.Common
{
    /// <summary>
    /// Class to help with common methods across different Controllers.
    /// </summary>
    public class DSXHelper
    {
        //Read only constants

        //Template File Paths
        private readonly string TemplatesPath = ConfigHelper.GetStringValue("TemplatesPath"),
            GrantAccessTemplateFileName = ConfigHelper.GetStringValue("GrantAccessTemplateFileName"),
            RevokeAccessTemplateFileName = ConfigHelper.GetStringValue("RevokeAccessTemplateFileName"),
            ChangePinTemplateFileName = ConfigHelper.GetStringValue("ChangePINTemplateFileName"),
            ReplaceCardTemplateFileName = ConfigHelper.GetStringValue("ReplaceCardTemplateFileName");

        //DML Directory Paths
        private readonly string DMLDestinationPath = ConfigHelper.GetStringValue("DMLDestinationPath"),
            DMLArchiveDestinationPath = ConfigHelper.GetStringValue("DMLArchiveDestinationPath");

        //File Names
        private readonly string GrantAccessFileName = ConfigHelper.GetStringValue("GrantAccessFileName"),
            RevokeAccessFileName = ConfigHelper.GetStringValue("RevokeAccessFileName"),
            ChangePinFileName = ConfigHelper.GetStringValue("ChangePINFileName"),
            ReplaceCardFileName = ConfigHelper.GetStringValue("ReplaceCardFileName");

        /// <summary>
        /// Creates a DML Object and assigns all paths from the Web.Config File
        /// </summary>
        /// <returns>A DSX.DML object</returns>
        public DML CreateDmlObject()
        {
            var result = new DML(DMLDestinationPath, DMLArchiveDestinationPath)
            {
                TemplatesPath = TemplatesPath,

                ChangePinTemplateFileName = ChangePinTemplateFileName,
                GrantAccessTemplateFileName = GrantAccessTemplateFileName,
                RevokeAccessTemplateFileName = RevokeAccessTemplateFileName,
                ReplaceCardTemplateFileName = ReplaceCardTemplateFileName,

                GrantAccessFileName = GrantAccessFileName,
                ChangePinFileName = ChangePinFileName,
                RevokeAccessFileName = RevokeAccessFileName,
                ReplaceCardFileName = ReplaceCardFileName,
                
            };

            return result;
        }
    }
}