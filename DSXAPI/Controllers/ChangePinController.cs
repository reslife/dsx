﻿using DSXAPI.Common;
using NLog;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace DSXAPI.Controllers
{
    /// <summary>
    /// This Controller Changes a PIN in DSX
    /// </summary>
    public class ChangePinController : ApiController
    {
        // POST api/values

        /// <summary>
        /// Creates a ChangePin DML file.
        /// </summary>
        /// <param name="value">The Json object containing the CardHolder data. e.g. "
        /// "{StudentID:'00000000', CatCardNumber:'90200000000', PIN:'0000'}"
        /// *StudentID is optional</param>
        public HttpResponseMessage Post(HttpRequestMessage value)
        {
            var h = new DsxApiHelper(value, User.Identity.Name);
            return h.ProcessDsxRequest(DsxApiHelper.RequestType.ChangePin);
        }
    }
}
