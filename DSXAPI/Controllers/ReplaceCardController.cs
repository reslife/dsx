﻿using DSXAPI.Common;
using NLog;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;


namespace DSXAPI.Controllers
{
    /// <summary>
    /// This Controller Replaces a Card in DSX
    /// </summary>
    public class ReplaceCardController : ApiController
    {
        // POST api/values

        /// <summary>
        /// Creates a ReplaceCard DML file.
        /// </summary>
        /// <param name="value">The Json object containing the CardHolder data. e.g. "
        /// ""{StudentID:'00000000', CatCardNumber:'90200000000'}""</param>
        public HttpResponseMessage Post(HttpRequestMessage value)
        {
            var h = new DsxApiHelper(value, User.Identity.Name);
            return h.ProcessDsxRequest(DsxApiHelper.RequestType.ReplaceCard);
        }
    }
}
