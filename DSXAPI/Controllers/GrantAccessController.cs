﻿using DSXAPI.Common;
using System;
using System.Net.Http;
using System.Web.Http;

namespace DSXAPI.Controllers
{
    /// <summary>
    /// This Controller Grants Access to DSX
    /// </summary>
    public class GrantAccessController : ApiController
    {
        // POST api/values

        /// <summary>
        /// Creates a Grant Access DML file.
        /// </summary>
        /// <param name="value">The Json object containing the CardHolder data. e.g. "
        /// {StudentID: '00000000', FirstName: 'John', LastName: 'Doe', Company: 'HNRV Student', 
        /// CatCardNumber:'90200000000', AccessLevel: 'HNRV Student', PIN: '0000',
        /// StartDate: '1/1/2020 15:00', EndDate: '12/31/8888 12:00'}"
        /// *StudentID is optional.
        /// *PIN is optional.</param>
        public HttpResponseMessage Post(HttpRequestMessage value)
        {
            var h = new DsxApiHelper(value, User.Identity.Name);
            return h.ProcessDsxRequest(DsxApiHelper.RequestType.GrantAccess);
        }
    }
}
