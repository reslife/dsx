﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSXAPI.Models
{
    /// <summary>
    /// This class is purely used for documentation
    /// </summary>
    public class RevokeAccessRequest
    {
        /// <summary>
        /// The Student ID. This field is optiona.
        /// </summary>
        public string StudentID { get; set; }
        /// <summary>
        /// The Cat Card Number
        /// </summary>
        public string CatCardNumber { get; set; }
        /// <summary>
        /// The access level name to grant access to.
        /// </summary>
        public string AccessLevel { get; set; }

    }
}