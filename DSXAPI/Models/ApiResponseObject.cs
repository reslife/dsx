﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSXAPI.Models
{
    /// <summary>
    /// This class is intended for documentation purposes only
    /// </summary>
    public class ApiResponseObject
    {
        /// <summary>
        /// The result object
        /// </summary>
        public Result Result { get; set; }
    }

    /// <summary>
    /// This class is intended for documentation purposes only
    /// </summary>
    public class Result
    {
        /// <summary>
        /// The status of the request. Posible values: "Success", "Error".
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// The user who requested the DSX API
        /// </summary>
        public string User { get; set; }
        /// <summary>
        /// The Request type. Possible Values: "GrantAccess", "RevokeAccess", "ChangePin" and "ReplaceCard"
        /// </summary>
        public string RequestType { get; set; }
        /// <summary>
        /// The Message sent by the API
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// The original data sent to the API.
        /// </summary>
        public Object OriginalData { get; set; }

    }
}