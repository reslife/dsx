﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSXAPI.Models
{
 
    /// <summary>
    /// This class is purely used for documentation
    /// </summary>
    public class GrantAccessRequest
    {
        /// <summary>
        /// The Student ID. This field is optional.
        /// </summary>
        public string StudentID { get; set; }
        /// <summary>
        /// The First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// The Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The company name
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// The Cat Card Number
        /// </summary>
        public string CatCardNumber { get; set; }
        /// <summary>
        /// The access level name to grant access to.
        /// </summary>
        public string AccessLevel { get; set; }
        /// <summary>
        /// The Personal Identification Number of the card. This field is optional.
        /// </summary>
        public string PIN { get; set; }
        /// <summary>
        /// The date time the access will begint at. The format must be M/d/yyyy HH:mm
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// The date the access will expire. The format must be M/d/yyyy HH:mm
        /// </summary>
        public string EndDate { get; set; }

    }
}